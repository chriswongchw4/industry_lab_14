package ictgradschool.industry.lab14.ex05;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * A shape which is capable of loading and rendering images from files / Uris / URLs / etc.
 */
public class ImageShape extends Shape {

    private Image image;

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, String fileName) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, new File(fileName).toURI());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URI uri) throws MalformedURLException {
        this(x, y, deltaX, deltaY, width, height, uri.toURL());
    }

    public ImageShape(int x, int y, int deltaX, int deltaY, int width, int height, URL url) {
        super(x, y, deltaX, deltaY, width, height);

        // Create the placeholder image
        Image i = new BufferedImage(200, 200, BufferedImage.SCALE_SMOOTH);
        i.getGraphics().setColor(Color.pink);
        i.getGraphics().drawRect(0, 0, 200, 200);
        i.getGraphics().setColor(Color.black);
        i.getGraphics().drawString("Loading", 20, 100);
        image = i;


        SwingWorker<Image,Void> imageWorker = new SwingWorker<Image, Void>() {

            Image finalImage = null;

            @Override
            protected Image doInBackground() throws Exception {

                try {
                    Image image = ImageIO.read(url);
                    if (width == image.getWidth(null) && height == image.getHeight(null)) {
                        finalImage = image;
                    } else {
                        finalImage = image.getScaledInstance(width, height, Image.SCALE_SMOOTH);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return finalImage;
            }

            protected void done(){
                try {
                    image = get();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        };

        imageWorker.execute();

        // Create a SwingWorker that loads the image from the URL
        //  when completed, replace the placeholder with the real thing


    }


    @Override
    public void paint(Painter painter) {

        painter.drawImage(this.image, fX, fY, fWidth, fHeight);

    }
}
