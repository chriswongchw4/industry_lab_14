package ictgradschool.industry.lab14.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Simple application to calculate the prime factors of a given number N.
 * <p>
 * The application allows the user to enter a value for N, and then calculates
 * and displays the prime factors. This is a very simple Swing application that
 * performs all processing on the Event Dispatch thread.
 */
public class PrimeFactorsSwingApp extends JPanel {

    private JButton _startBtn;        // Button to start the calculation process.
    private JButton _abortBtn;
    private JTextArea _factorValues;  // Component to display the result.

    private class PrimeFactorisationWorker extends SwingWorker<ArrayList<Long>, Long> {
        private long _number;

        public PrimeFactorisationWorker(long targetNum) {
            _number = targetNum;
        }



        @Override
        protected ArrayList<Long> doInBackground() throws Exception {
            ArrayList<Long> primeFactors = new ArrayList<>();


            if (!isCancelled()) {
                for (long i = 2; i * i <= _number; i++) {

                    if (isCancelled()) {
                        break;
                    }
                    // If i is a factor of N, repeatedly divide it out
                    while (_number % i == 0) {
                        publish(i);
                        Thread.sleep(1000);
                        _number = _number / i;
                    }
                }

                // if biggest factor occurs only once, n > 1
                if (_number > 1) {
                    publish(_number);
                }

                return primeFactors;
            }
            return null;
        }



        @Override
        public void done() {


            // Re-enable the Start button.
            _startBtn.setEnabled(true);

            _abortBtn.setEnabled(false);
            // Restore the cursor.
            setCursor(Cursor.getDefaultCursor());

        }

        @Override
        protected void process(java.util.List<Long> primeFactors){
            for (Long l : primeFactors) {
                _factorValues.append(l + "\n");
            }
        }
    }

    PrimeFactorisationWorker primeWorker;


    public PrimeFactorsSwingApp() {
        // Create the GUI components.
        JLabel lblN = new JLabel("Value N:");
        final JTextField tfN = new JTextField(20);
        PrimeFactorisationWorker test;

        _startBtn = new JButton("Compute");
        _abortBtn = new JButton("Abort");
        _factorValues = new JTextArea();
        _factorValues.setEditable(false);

        // Add an ActionListener to the start button. When clicked, the
        // button's handler extracts the value for N entered by the user from
        // the textfield and find N's prime factors.


        _startBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent event) {
                String strN = tfN.getText().trim();
                long n = 0;

                try {
                    n = Long.parseLong(strN);
                } catch (NumberFormatException e) {
                    System.out.println(e);
                }


                primeWorker = new PrimeFactorisationWorker(n);

                primeWorker.execute();


                // Disable the Start button until the result of the calculation is known.
                _startBtn.setEnabled(false);
                _abortBtn.setEnabled(true);

                // Clear any text (prime factors) from the results area.
                _factorValues.setText(null);

                // Set the cursor to busy.
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));


            }
        });

        _abortBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    primeWorker.cancel(true);
                } catch (Exception ex) {

                }


                _startBtn.setEnabled(true);
                _abortBtn.setEnabled(false);

                _factorValues.append("\ncancelled by user\n");
                setCursor(Cursor.getDefaultCursor());
            }
        });

        // Construct the GUI.
        JPanel controlPanel = new JPanel();
        controlPanel.add(lblN);
        controlPanel.add(tfN);
        controlPanel.add(_startBtn);
        controlPanel.add(_abortBtn);
        _abortBtn.setEnabled(false);

        JScrollPane scrollPaneForOutput = new JScrollPane();
        scrollPaneForOutput.setViewportView(_factorValues);

        setLayout(new BorderLayout());
        add(controlPanel, BorderLayout.NORTH);
        add(scrollPaneForOutput, BorderLayout.CENTER);
        setPreferredSize(new Dimension(500, 300));
    }


    private static void createAndShowGUI() {
        // Create and set up the window.
        JFrame frame = new JFrame("Prime Factorisation of N");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create and set up the content pane.
        JComponent newContentPane = new PrimeFactorsSwingApp();
        frame.add(newContentPane);

        // Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void main(String[] args) {
        // Schedule a job for the event-dispatching thread:
        // creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}

